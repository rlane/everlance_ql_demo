import { GraphQLServer } from 'graphql-yoga'

// Scalar types - String, Boolean, Int, Float, ID

// Type definitions (schema)
const typeDefs = `
    type Query {
        greeting(name: String, position: String): String!
        add(numbers: [Float!]!): Float!
        grades: [Int!]!
        me: User!
        post: Post!
        users: [User!]!
    }

    type User { 
        id: ID!
        name: String!
        email: String!
        age: Int
    }
    
    type Post { 
        id: ID!
        title: String!
        body: String!
        published: Boolean!
    }
`

// Resolvers
const resolvers = { 
    Query: {
        greeting(parent, args, ctx, info) {
            if  (args.name && args.position) { 
                return `Hello, ${args.name}! You are my favorite ${args.position}.`
            }  else { 
                return 'Hello!'
            }
        },
        add(parent, args, ctx, info) { 
            if (args.numbers.length === 0) { 
                return 0;
            } 
            return args.numbers.reduce((accumulator, currentValue) => { 
                return accumulator + currentValue
            });
        },
        grades(parent, args, ctx, info) { 
            return [99, 80, 93]
        },
        me() { 
            return { 
                id: '123098',
                name: 'Mike',
                email: 'mike@example.com',
                age: null
            }
        },
        post() { 
            return { 
                id: 'xyz432',
                title: 'Start your day with a cup of Joe!',
                body: "If you don't start your day with a cup of the best coffee, well you're just doing it wrong.",
                published: true
            }
        }
    }
}

const server = new GraphQLServer({
    typeDefs: typeDefs,
    resolvers: resolvers
})

server.start(() => { 
    console.log('The server is up')
})

process.on('SIGINT', () => { console.log("Bye bye!"); process.exit(); });

